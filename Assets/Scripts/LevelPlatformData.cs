﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataPlatform", menuName = "Object/LevelPlatform", order = 2)]
public class LevelPlatformData : ScriptableObject
{
    public string Name;
    public int Index;
    public int Width;
    public bool IsUpPlatform;
    public float HeightUp;
    public float WidthPit;
    public bool IsFirstPlatform;
    public Vector3 ParentPosition;

    public LevelPlatformData(string name, int index, int width, bool isUpPlatform, float heightUp, float widthPit, bool isFirstPlatform, Vector3 parentPosition)
    {
        Name = name;
        Index = index;
        Width = width;
        IsUpPlatform = isUpPlatform;
        HeightUp = heightUp;
        WidthPit = widthPit;
        IsFirstPlatform = isFirstPlatform;
        ParentPosition = parentPosition;
    }
    public LevelPlatformData(LevelPlatformData platformData)
    {
        //var platformData = new LevelPlatformData("Platform", indexPlatform, width, isUp, heightUp, widthPit, isFirstPlatform, positionNextPlatform);
        Name = platformData.Name;
        Index = platformData.Index;
        Width = platformData.Width;
        IsUpPlatform = platformData.IsUpPlatform;
        HeightUp = platformData.HeightUp;
        WidthPit = platformData.WidthPit;
        IsFirstPlatform = platformData.IsFirstPlatform;
        ParentPosition = platformData.ParentPosition;
    }
    //public LevelPlatformData CreateLevelPlatformData(LevelPlatformData platformData)
    //{
    //    var newPlatformData = new LevelPlatformData(platformData.Name, platformData.Index, platformData.Width, platformData.IsUpPlatform, platformData.HeightUp, platformData.WidthPit, platformData.IsFirstPlatform, platformData.ParentPosition);

    //}
}

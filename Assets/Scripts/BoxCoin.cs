﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCoin : MonoBehaviour, IHitBox
{

    [SerializeField] private Animator animator;
    [SerializeField] private GameObject coinPrefabs;
    [SerializeField] private float force;
    private int health = 4;
    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    public void Die()
    {
        StartCoroutine(Destroy());
    }

    public void Hit(int damage)
    {
        Health -= damage;
        animator.SetInteger("Health", Health);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out var player))
        {
            Hit(1);
        }

    }
    public void LaunchCoin()
    {
        var randomCoin = Random.Range(2, 6);
        for (int i = 0; i < randomCoin; i++)
        {
            var obj = Instantiate(coinPrefabs, transform.position, transform.rotation);
            var rig = obj.GetComponent<Rigidbody2D>();
            var randomDirection = Random.Range(-1f, 1f);
            rig.AddForce(new Vector2(force * randomDirection, force), ForceMode2D.Impulse);
        }
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(0.1f);
        var colliders = GetComponents<BoxCollider2D>();
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].enabled = false;
        }
        LaunchCoin();
        yield return new WaitForSeconds(0.35f);
        Destroy(gameObject);
    }
}

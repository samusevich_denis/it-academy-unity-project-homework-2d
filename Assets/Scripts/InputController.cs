﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static float HorizontalAxis;
    public static bool IsRun;
    public static bool IsRangeAttack;
    public static event Action<float> JumpAction;
    public static event Action<string,float> FireAction;
    private float rangeAttackTimer;
    private float jumpTimer;
    private Coroutine waitForJumpCoroutine;
    private Coroutine waitForRangeAttackCoroutine;

    // Start is called before the first frame update
    void Start()
    {
        HorizontalAxis = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        IsRun = Input.GetKey(KeyCode.LeftShift);
        HorizontalAxis = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump"))
        {
            if (waitForJumpCoroutine == null)
            {
                waitForJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }
            jumpTimer = Time.time;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireAction?.Invoke("Fire1",1);
        }

        if (Input.GetButtonDown("Fire2"))
        {
            if (waitForRangeAttackCoroutine == null)
            {
                waitForRangeAttackCoroutine = StartCoroutine(WaitRangeAttack());
            }
            rangeAttackTimer = Time.time;
        }
    }

    private void OnDestroy()
    {
        HorizontalAxis = 0f;
    }
    private IEnumerator WaitJump()
    {
        yield return new WaitForSeconds(0.2f);
        if (JumpAction != null)
        {
            float force = Time.time - jumpTimer < 0.2f ? 1.25f : 1f;//TODO  это должно быть в константах
            JumpAction.Invoke(force);
        }
        waitForJumpCoroutine = null;
    }
    private IEnumerator WaitRangeAttack()
    {
        IsRangeAttack = true;
        while (Input.GetButton("Fire2"))
        {
            yield return new WaitForSeconds(0.1f);
        }
        if (FireAction != null)
        {

            float force = Time.time - rangeAttackTimer < 1f ? Time.time - rangeAttackTimer : 1f;
            FireAction?.Invoke("Fire2",force);
        }
        IsRangeAttack = false;
        waitForRangeAttackCoroutine = null;
    }
}

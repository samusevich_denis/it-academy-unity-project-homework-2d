﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : CharacterMovement
{

    [SerializeField] private float maxSpeed = 1f;
    [SerializeField] private Transform graphics;
    [SerializeField] private float jumpForce = 5f;
    [SerializeField] private Animator animator;
    private Rigidbody2D rig;
    private static readonly int Speed = Animator.StringToHash("Speed");
    private static readonly int Jumped = Animator.StringToHash("Jumped");
    private static readonly int InAir = Animator.StringToHash("InAir");
    private static readonly int Run = Animator.StringToHash("Run");
    private static readonly int IsRangeAttack = Animator.StringToHash("IsRangeAttack");
    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        InputController.JumpAction += OnJumpAction;
    }

    private void FixedUpdate()
    {
        if (IsFreezing)
        {
            Vector2 velocity = rig.velocity;
            velocity.x = 0f;
            rig.velocity = velocity;
            return;
        }

        var direction = new Vector2(InputController.HorizontalAxis, 0f);

        if (!IsGrounded())
        {
            direction *= 0.5f;
        }
        Move(direction);
    }

    private void Update()
    {
        if (IsGrounded())
        {
            animator.SetBool(Run, InputController.IsRun);
            animator.SetFloat(Speed, Mathf.Abs(rig.velocity.x));
        }
        else
        {
            animator.SetFloat(Speed, 0);
        }
        if (PlayerInAir())
        {
            animator.SetBool(InAir, true);
        }
        else
        {
            animator.SetBool(InAir, false);
        }
        if (IsRangeAttackPlayer())
        {
            animator.SetBool(IsRangeAttack, true);
        }
        else
        {
            animator.SetBool(IsRangeAttack, false);
        }
        if (Mathf.Abs(rig.velocity.x) < 0.01f)
        {
            return;
        }

        var angle = rig.velocity.x > 0f ? 0f : 180f;
        graphics.localEulerAngles = new Vector3(0f, angle, 0f);

    }

    public override void Move(Vector2 direction)
    {
        Vector2 velocity = rig.velocity;
        if (InputController.IsRun)
        {
            velocity.x = direction.x * maxSpeed*1.5f;
        }
        else
        {
            velocity.x = direction.x * maxSpeed;
        }
        rig.velocity = velocity;
    }

    public override void Stop(float timer)
    {

    }

    public override void Jump(float force)
    {
        animator.SetTrigger(Jumped);
        rig.AddForce(new Vector2(0, force), ForceMode2D.Impulse);
    }

    private void OnJumpAction(float force)
    {
        if (IsGrounded() && !IsFreezing)
        {
            Jump(jumpForce * force);
        }
    }

    private bool IsGrounded()
    {
        Vector2 point = transform.position;
        point.y -= 0.1f; //чтобы избежать пересечения с самим собой
        RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, 0.2f);

        return hit.collider != null;
    }
    private bool PlayerInAir()
    {
        Vector2 point = transform.position;
        point.y -= 0.1f; //чтобы избежать пересечения с самим собой
        RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, 0.2f);
        return hit.collider == null;
    }
    private bool IsRangeAttackPlayer()
    {
        return InputController.IsRangeAttack;
    }
}

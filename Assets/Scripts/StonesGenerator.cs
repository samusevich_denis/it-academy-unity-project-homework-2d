﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StonesGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private float forse;
    [SerializeField] private int damage = 1;
    [SerializeField] private Animator animatorVolcano;
    [SerializeField] private float animationDelay = 0.28f;
    private static readonly int Eruption = Animator.StringToHash("Eruption");
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StonesCreatorProcess());
    }

    private IEnumerator StonesCreatorProcess()
    {
        while (true)
        {
            var randomTime = Random.Range(2f,5f);
            yield return new WaitForSeconds(randomTime);
            animatorVolcano.SetTrigger(Eruption);
            yield return new WaitForSeconds(animationDelay);
            var current = Random.Range(0, prefabs.Length);
            GameObject obj = Instantiate(prefabs[current]);
            var projectileMovement = obj.GetComponent<ProjectileMovement>();
            projectileMovement.LaunchBulletBackground(forse, damage, new Vector2(0f,1f));
            obj.transform.position = transform.position;
        }
    }
}

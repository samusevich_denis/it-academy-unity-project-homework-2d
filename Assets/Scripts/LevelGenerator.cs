﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private Transform environmentGrid;
    [SerializeField] private Tilemap[] tilemapPlatforms;
    [SerializeField] private Tilemap[] tilemapSmallPlatforms;
    [SerializeField] private Tilemap[] tilemapBackground;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private SpriteRenderer[] spriteBackground;

    private const float width = 19.2f;
    private const float widthBackground = 13.34f;
    private List<int> levelData;
    private List<Tilemap> tilemapsPlatformsActive;
    private List<Tilemap> tilemapSmallPlatformsActive;
    private List<Tilemap> tilemapBackgroundActive;
    private int playerCurentPosition;
    private int playerPosition;
    private int playerIndexPosition;
    private float positionTilemapRightX;
    private float positionTilemapLeftX;
    void Start()
    {
        levelData = new List<int>();
        tilemapsPlatformsActive = new List<Tilemap>();
        tilemapSmallPlatformsActive = new List<Tilemap>();
        tilemapBackgroundActive = new List<Tilemap>();
        CreateObjectLevel(0, 0f,true);
        levelData.Add(0);
        levelData.Add(CreateRandomObjectLevel(width,true));
        levelData.Insert(0,CreateRandomObjectLevel(-width*2,false));
        playerIndexPosition =1;
        positionTilemapRightX = width*2;
        positionTilemapLeftX = -width*3;
    }

    void Update()
    {
        playerCurentPosition = (int)(playerTransform.position.x / width);
        var change = playerCurentPosition - playerPosition;
        if (change == 0)
        {
            return;
        }
        if (change == 1)
        {
            CreateRightAndDeleteObjectLevelLeft();
        }
        if (change == -1)
        {
            CreateLeftAndDeleteObjectLevelRight();
        }
        playerPosition = playerCurentPosition;
        MoveBackgruond();
    }

    private void MoveBackgruond()
    {
        for (int i = 0; i < spriteBackground.Length; i++)
        {
            var position = spriteBackground[i].transform.position;
            position.x = ((int)(playerTransform.position.x / widthBackground))* widthBackground;
            spriteBackground[i].transform.position = position;
        }
    }

    private void CreateRightAndDeleteObjectLevelLeft()
    {
        playerIndexPosition++;
        if (playerIndexPosition == levelData.Count-1)
        {
            levelData.Add(CreateRandomObjectLevel(positionTilemapRightX, true));
        }
        else
        {
            CreateObjectLevel(levelData[playerIndexPosition + 1], positionTilemapRightX, true);
        }
        DeleteObjectLevel(false);
        positionTilemapRightX += width* (playerCurentPosition == -2 ? 2 : 1);
        positionTilemapLeftX += width * (playerCurentPosition == 2 ? 2 : 1);
    }

    private void CreateLeftAndDeleteObjectLevelRight()
    {
        playerIndexPosition--;
        if (playerIndexPosition == 0)
        {
            levelData.Insert(0,CreateRandomObjectLevel(positionTilemapLeftX, false));
            playerIndexPosition++;
        }
        else
        {
            CreateObjectLevel(levelData[playerIndexPosition- 1], positionTilemapLeftX, false);
        }
        DeleteObjectLevel(true);
        positionTilemapRightX -= width*(playerCurentPosition == -3 ? 2 : 1);
        positionTilemapLeftX -= width*(playerCurentPosition == 1 ? 2 : 1);
    }

    private int CreateRandomObjectLevel(float positionX, bool isRight)
    {
        var random = Random.Range(1, tilemapPlatforms.Length);
        CreateObjectLevel(random, positionX, isRight);
        return random;
    }

    private void DeleteObjectLevel(bool isRight)
    {
        if (isRight)
        {
            DeleteObject(tilemapsPlatformsActive, tilemapsPlatformsActive.Count - 1);
            DeleteObject(tilemapSmallPlatformsActive, tilemapSmallPlatformsActive.Count - 1);
            DeleteObject(tilemapBackgroundActive, tilemapBackgroundActive.Count - 1);
        }
        else
        {
            DeleteObject(tilemapsPlatformsActive, 0);
            DeleteObject(tilemapSmallPlatformsActive, 0);
            DeleteObject(tilemapBackgroundActive, 0);
        }
    }

    private void DeleteObject(List<Tilemap> tilemapsActive, int index)
    {
        Destroy(tilemapsActive[index].transform.gameObject);
        tilemapsActive.RemoveAt(index);
    }

    private void CreateObjectLevel(int numberTilemap, float positionX, bool isRight)
    {
        if (isRight)
        {
            tilemapsPlatformsActive.Add(CreateObject(tilemapPlatforms[numberTilemap], positionX));
            tilemapSmallPlatformsActive.Add(CreateObject(tilemapSmallPlatforms[numberTilemap], positionX));
            tilemapBackgroundActive.Add(CreateObject(tilemapBackground[numberTilemap], positionX));
        }
        else
        {
            tilemapsPlatformsActive.Insert(0, CreateObject(tilemapPlatforms[numberTilemap], positionX));
            tilemapSmallPlatformsActive.Insert(0, CreateObject(tilemapSmallPlatforms[numberTilemap], positionX));
            tilemapBackgroundActive.Insert(0,CreateObject(tilemapBackground[numberTilemap], positionX));
        }
    }

    private Tilemap CreateObject(Tilemap tilemap, float positionX)
    {
        var objectPlatform = Instantiate(tilemap, environmentGrid);
        objectPlatform.transform.position = new Vector3(positionX, 0f, 0f);
        return objectPlatform;
    }

}

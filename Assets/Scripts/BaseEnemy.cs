﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EEnemyState
{
    Sleep,
    Wait,
    StartWalk,
    Walk,
    StartPatrol,
    Patrol,
    StartAttack,
    Attack,
    StartFear,
    Fear,
    Jump,
}

public class BaseEnemy : MonoBehaviour, IEnemy, IHitBox
{
    //State mashine
    [SerializeField] private Animator animator;
    [SerializeField] private Transform checkGroundPoint;
    [SerializeField] private Transform checkAttackPoint;
    [SerializeField] private Transform checkNearestJumpPoint;
    [SerializeField] private Transform checkDistantJumpPoint;
    [SerializeField] private Transform graphics;
    [SerializeField] private int health = 10;

    private GameManager gameManager;

    private EEnemyState currentState = EEnemyState.Sleep;

    private float wakeUpTimer;
    private float waitTimer;
    private float fearTimer;
    private float patrolTimer;
    private float patroWaitTimer;
    private float attackTimer;
    private float jumpTimer;
    private bool isWaitPotrol;
    private EEnemyState nextState;
    private float currentDirection = 1f;
    private bool? jumpNearestRight;
    private bool? jumpNearestLeft;
    private bool? jumpDistantRight;
    private bool? jumpDistantLeft;
    private float iteration;
    private float distanse;
    private Rigidbody2D rig;
    float targetJumpX;
    Vector2 velocityStartJump;

    public void RegisterEnemy()
    {
        gameManager = FindObjectOfType<GameManager>();
        rig = GetComponent<Rigidbody2D>();
        gameManager.Enemies.Add(this);
    }

    private void Awake()
    {
        animator.SetBool("IsGround", true);
        RegisterEnemy();
        wakeUpTimer = Time.time + 1f;
    }

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public void Hit(int damage)
    {
        Health -= damage;
        currentState = EEnemyState.StartFear;
    }

    public void Die()
    {
        animator.SetTrigger("Die");
        Destroy(this);
        Destroy(gameObject, 1f);
    }

    private void Update()
    {
        print(UnityEngine.Random.Range(0, 2).ToString());
        switch (currentState)
        {
            case EEnemyState.Sleep:
                Sleep();
                break;
            case EEnemyState.Wait:
                Wait();
                break;
            case EEnemyState.StartPatrol:
                animator.SetInteger("Walking", 0);
                currentState = EEnemyState.Patrol;
                break;
            case EEnemyState.Patrol:
                Patrol();
                break;
            case EEnemyState.StartWalk:
                animator.SetInteger("Walking", 1);
                currentState = EEnemyState.Walk;
                break;
            case EEnemyState.Walk:
                Walk();
                break;
            case EEnemyState.StartAttack:
                animator.SetTrigger("Attack");
                ((IHitBox)gameManager.Player).Hit(1);
                currentState = EEnemyState.Attack;
                break;
            case EEnemyState.Jump:
                Jump(distanse);
                break;
            case EEnemyState.Attack:
                Attack();
                break;

            case EEnemyState.StartFear:
                StartFear();
                break;
            case EEnemyState.Fear:
                Fear();
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void StartSleeping(float sleepTime = 1f)
    {
        wakeUpTimer = Time.time + sleepTime;
        currentState = EEnemyState.Sleep;
    }

    private void Sleep()
    {
        if (Time.time >= wakeUpTimer)
        {
            WakeUp();
        }
    }

    private void WakeUp()
    {
        var playerPosition = ((MonoBehaviour)gameManager.Player).transform.position;

        if (Vector3.Distance(transform.position, playerPosition) > 20f)
        {
            StartSleeping();
            return;
        }

        if (Mathf.Abs(playerPosition.y - transform.position.y) < 0.3)
        {
            currentState = EEnemyState.Wait;
            nextState = EEnemyState.StartWalk;
            waitTimer = Time.time + 0.1f;
            return;
        }
        else
        {
            currentState = EEnemyState.Wait;
            nextState = EEnemyState.StartPatrol;
            isWaitPotrol = true;
            waitTimer = Time.time + 0.3f;
            patroWaitTimer = Time.time + 1.5f;
            animator.SetInteger("Walking", 0);
            return;
        }
    }

    private void Wait()
    {
        if (Time.time >= waitTimer)
        {
            currentState = nextState;
        }
    }

    private void Patrol()
    {
        var playerPosition = ((MonoBehaviour)gameManager.Player).transform.position;
        if (Mathf.Abs(playerPosition.y - transform.position.y) < 0.3)
        {
            currentState = EEnemyState.Wait;
            nextState = EEnemyState.StartWalk;
            waitTimer = Time.time + 0.1f;
            return;
        }

        if (isWaitPotrol)
        {
            if (Time.time < patroWaitTimer)
            {
                return;
            }
            else
            {
                isWaitPotrol = false;
                patrolTimer = Time.time + 3f;
                animator.SetInteger("Walking", 1);
            }
        }
        else
        {
            RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, Vector2.down, 0.3f);
            if (hit.collider == null)
            {
                if (TryJump())
                {
                    StartJump();
                    return;
                }
                else
                {
                    currentDirection *= -1;
                    float angle = currentDirection > 0 ? 0f : 180f;
                    graphics.localEulerAngles = new Vector3(0, angle, 0f);
                    return;
                }
            }
            transform.Translate(transform.right * Time.deltaTime * currentDirection);
        }

        if (Time.time > patrolTimer)
        {
            isWaitPotrol = true;
            patroWaitTimer = Time.time + 1.5f;
            animator.SetInteger("Walking", 0);
            return;
        }
    }

    private void Walk()
    {
        var playerPosition = ((MonoBehaviour)gameManager.Player).transform.position;

        if (Mathf.Abs(playerPosition.y - transform.position.y) > 0.3)
        {
            currentState = EEnemyState.Wait;
            nextState = EEnemyState.StartPatrol;
            isWaitPotrol = true;
            waitTimer = Time.time + 0.3f;
            patroWaitTimer = Time.time + 1.5f;
            animator.SetInteger("Walking", 0);
            return;
        }

        transform.Translate(transform.right * Time.deltaTime * currentDirection);

        RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, Vector2.down, 0.3f);
        if (hit.collider == null)
        {
            if (TryJump())
            {
                StartJump();
                return;
            }
            else
            {
                currentDirection *= -1;
                float angle = currentDirection > 0 ? 0f : 180f;
                graphics.localEulerAngles = new Vector3(0, angle, 0f);
                currentState = EEnemyState.Wait;
                waitTimer = Time.time + 0.3f;
                animator.SetInteger("Walking", 0);
                return;
            }
        }

        hit = Physics2D.Raycast(checkAttackPoint.position, Vector2.right, 0.3f);
        if (hit.collider == null)
        {
            return;
        }
        var player = hit.collider.GetComponent<Player>();
        if (player)
        {
            currentState = EEnemyState.StartAttack;
        }
    }

    void Attack()
    {
        if (Time.time < attackTimer)
        {
            return;
        }
        currentState = EEnemyState.Wait;
        nextState = EEnemyState.StartWalk;
        waitTimer = Time.time + 0.2f;
    }

    private void StartFear()
    {

        fearTimer = Time.time + 5f;
        currentState = EEnemyState.Fear;
    }
    private void Fear()
    {
        if (fearTimer < Time.time)
        {
            animator.SetInteger("Walking", 0);
            currentState = EEnemyState.Wait;
            nextState = EEnemyState.StartWalk;
            waitTimer = Time.time + 0.3f;
            return;
        }
        var point = transform.InverseTransformPoint(((MonoBehaviour)gameManager.Player).transform.position);
        if (point.x > 0 && currentDirection > 0)
        {
            currentDirection *= -1;
        }
        if (point.x < 0 && currentDirection < 0)
        {
            currentDirection *= -1;
        }
        float angle = currentDirection > 0 ? 0f : 180f;
        graphics.localEulerAngles = new Vector3(0, angle, 0f);
        RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, Vector2.down, 0.3f);
        if (hit.collider == null)
        {
            animator.SetInteger("Walking", 0);
        }
        else
        {
            animator.SetInteger("Walking", 2);
            transform.Translate(transform.right * Time.deltaTime * currentDirection * 3);
        }
    }

    private bool TryJump()
    {
        if (currentDirection > 0)
        {
            if (jumpNearestRight.HasValue)
            {
                var rand = UnityEngine.Random.Range(0, 3);
                if (rand == 1)
                {
                    distanse = 1.28f;
                    return jumpNearestRight.Value;
                }
                else if (rand == 2)
                {
                    distanse = 2.56f;
                    return jumpDistantRight.Value;
                }
            }
            else
            {
                CheckJump();
            }
        }
        else
        {
            if (jumpNearestLeft.HasValue)
            {
                var rand = UnityEngine.Random.Range(0, 3);
                if (rand == 1)
                {
                    distanse = 1.28f;
                    return jumpNearestLeft.Value;
                }
                else if (rand == 2)
                {
                    distanse = 2.56f;
                    return jumpDistantLeft.Value;
                }
            }
            else
            {
                CheckJump();
            }
        }
        return false;
    }
    private void CheckJump()
    {
        var nearestPosition = checkNearestJumpPoint.position;
        var distantPosition = checkDistantJumpPoint.position;
        RaycastHit2D hit = Physics2D.Raycast(nearestPosition, Vector2.down, 3);
        if (hit.collider != null)
        {
            if (currentDirection > 0)
            {
                jumpNearestRight = true;
            }
            else
            {
                jumpNearestLeft = true;
            }
        }
        else
        {
            if (currentDirection > 0)
            {
                jumpNearestRight = false;
            }
            else
            {
                jumpNearestLeft = false;
            }
        }
        hit = Physics2D.Raycast(distantPosition, Vector2.down, 3);
        if (hit.collider != null)
        {
            if (currentDirection > 0)
            {
                jumpDistantRight = true;
            }
            else
            {
                jumpDistantLeft = true;
            }
        }
        else
        {
            if (currentDirection > 0)
            {
                jumpDistantRight = false;
            }
            else
            {
                jumpDistantLeft = false;
            }
        }
    }
    private void StartJump()
    {
        animator.SetTrigger("Jump");
        animator.SetBool("IsGround", false);
        currentState = EEnemyState.Jump;
        targetJumpX = transform.position.x + distanse * currentDirection;
        jumpTimer = Time.time + 0.5f;
        if (currentDirection > 0)
        {
            if (jumpNearestRight.Value)
            {
                rig.AddForce(new Vector2(2* currentDirection, 10f), ForceMode2D.Impulse);
                velocityStartJump = rig.velocity;
                return;
            }
            if (jumpDistantRight.Value)
            {
                rig.AddForce(new Vector2(5 * currentDirection, 10f), ForceMode2D.Impulse);
                velocityStartJump = rig.velocity;
                return;
            }
        }
        else
        {
            if (jumpNearestLeft.Value)
            {
                rig.AddForce(new Vector2(2* currentDirection, 10f), ForceMode2D.Impulse);
                velocityStartJump = rig.velocity;
                return;
            }
            if (jumpDistantLeft.Value)
            {
                rig.AddForce(new Vector2(5* currentDirection, 10f), ForceMode2D.Impulse);
                velocityStartJump = rig.velocity;
                return;
            }
        }
    }




    private void Jump(float distanse)
    {
        var velocity  = rig.velocity;
        var posX = transform.position.x;
        velocity.x = velocityStartJump.x * Mathf.Abs(targetJumpX - posX) / distanse;
        rig.velocity = velocity;

        Vector2 point = transform.position;
        point.y -= 0.1f;
        RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, 0.2f);

        if (hit.collider != null && jumpTimer < Time.time)
        {
            animator.SetBool("IsGround", true);
            jumpNearestRight = null;
            jumpNearestLeft = null;
            jumpDistantRight = null;
            jumpDistantLeft = null;
            currentState = EEnemyState.Wait;
            nextState = EEnemyState.StartWalk;
            waitTimer = Time.time + 0.1f;

        }
    }
}
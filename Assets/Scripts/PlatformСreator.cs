﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class PlatformСreator : MonoBehaviour
{
    [SerializeField] private Sprite spriteLeftDown;
    [SerializeField] private Sprite spriteСenterDown;
    [SerializeField] private Sprite spriteRightDown;
    [SerializeField] private Sprite spriteLeftUp;
    [SerializeField] private Sprite spriteCenterUp;
    [SerializeField] private Sprite spriteRightUp;
    [Space]
    [Header("For line or random platforms")]
    [SerializeField] private int widthPlatforms = 7;
    [SerializeField] private bool isUpPlatforms = false;
    [SerializeField] private float widthPitPlatforms = 3;
    [SerializeField] private float heightUpPlatforms = 6;
    [SerializeField] private int countPlatforms = 5;
    [Space]
    [Header("For remove or add platforms")]
    [SerializeField] private int indexPlatformRemoveOrAdd = 0;
    private static int indexPlatform;
    private static bool isFirstPlatform;
    private static Vector3 positionNextPlatform;
    private static List<LevelPlatformData> levelPlatforms = new List<LevelPlatformData>();
    private static List<Transform> levelPlatformsObject = new List<Transform>();
    private static GameObject parentAllPlatform;


    [ContextMenu("Save platforms")]
    public void SavePlatforms()
    {
        for (int i = 0; i < levelPlatforms.Count; i++)
        {
            var savePlatformData = new LevelPlatformData(levelPlatforms[i]);
            CreateAsset<LevelPlatformData>(savePlatformData,i);
        }
    }
    [ContextMenu("Load platforms")]
    public void LoadPlatforms()
    {
        if (parentAllPlatform == null)
        {
            return;
        }
        var platformData = LoadAsset<LevelPlatformData>(0);

        for (int i = 1; platformData!=null; i++)
        {
            var newPlatformData = new LevelPlatformData(platformData);
            DeleteAsset<LevelPlatformData>(i-1);
            CreatePlatformFromData(newPlatformData);
            platformData = LoadAsset<LevelPlatformData>(i);
        }
    }
    [ContextMenu("Create platforms")]
    public void CreatePlatforms()
    {
        if (parentAllPlatform==null)
        {
            return;
        }
        for (int i = 0; i < countPlatforms; i++)
        {
            CreateOnePlatform(isUpPlatforms, heightUpPlatforms, widthPitPlatforms, widthPlatforms);
        }
    }
    [ContextMenu("Create random platforms")]
    public void CreateRandomPlatforms()
    {
        if (parentAllPlatform == null)
        {
            return;
        }
        for (int i = 0; i < countPlatforms; i++)
        {
            CreateOnePlatform(isUpPlatforms, Random.Range(2f, heightUpPlatforms), Random.Range(1f, widthPitPlatforms), Random.Range(2, widthPlatforms));
        }
    }
    [ContextMenu("Remove platform with index")]
    public void RemovePlatformWithIndex()
    {
        if (indexPlatformRemoveOrAdd<0|| indexPlatformRemoveOrAdd > levelPlatforms.Count-1 )
        {
            return;
        }
        var positionX = levelPlatformsObject[indexPlatformRemoveOrAdd].position.x;
        DestroyImmediate(levelPlatformsObject[indexPlatformRemoveOrAdd].gameObject);
        levelPlatforms.RemoveAt(indexPlatformRemoveOrAdd);
        levelPlatformsObject.RemoveAt(indexPlatformRemoveOrAdd);
        indexPlatform--;
        if (levelPlatforms.Count == indexPlatformRemoveOrAdd)
        {
            positionNextPlatform.x = positionX;
            return;
        }
        float distance = levelPlatformsObject[indexPlatformRemoveOrAdd].position.x - positionX;
        positionNextPlatform.x -= distance;
        for (int i = indexPlatformRemoveOrAdd; i < levelPlatforms.Count; i++)
        {
            levelPlatforms[i].Index--;
            levelPlatformsObject[i].name = $"Platform ({i})";
            if (levelPlatforms[i].IsFirstPlatform)
            {
                return;
            }
            var newPosition = levelPlatformsObject[i].position;
            newPosition.x -= distance;
            levelPlatformsObject[i].position = newPosition;
        }
    }
    [ContextMenu("Add platform with index")]
    public void AddPlatformWithIndex()
    {
        if (indexPlatformRemoveOrAdd < 0 || indexPlatformRemoveOrAdd > levelPlatforms.Count - 1)
        {
            return;
        }
        var platformData = new LevelPlatformData("Platform", indexPlatformRemoveOrAdd, widthPlatforms, isUpPlatforms, heightUpPlatforms, widthPitPlatforms, levelPlatforms[indexPlatformRemoveOrAdd].IsFirstPlatform, levelPlatformsObject[indexPlatformRemoveOrAdd].position);
        var positionLastPlatform = positionNextPlatform;
        InsertPlatformFromData(platformData, indexPlatformRemoveOrAdd);
        indexPlatform++;
        float distance = positionNextPlatform.x-levelPlatformsObject[indexPlatformRemoveOrAdd].position.x;
        positionLastPlatform.x += distance;
        positionNextPlatform = positionLastPlatform;
        for (int i = indexPlatformRemoveOrAdd+1; i < levelPlatforms.Count; i++)
        {
            levelPlatforms[i].Index++;
            levelPlatformsObject[i].name = $"Platform ({i})";
            if (levelPlatforms[i].IsFirstPlatform)
            {
                return;
            }
            var newPosition = levelPlatformsObject[i].position;
            newPosition.x += distance;
            levelPlatformsObject[i].position = newPosition;
        }
    }
    [ContextMenu("Set first position platform")]
    public void SetFirstPositionPlatform()
    {
        if (parentAllPlatform==null)
        {
            parentAllPlatform = new GameObject("All platforms");
        }
        positionNextPlatform = this.transform.position;
        isFirstPlatform = true;
    }
    private void Reset()
    {
        indexPlatform = 0;
        isFirstPlatform = true;
        positionNextPlatform = Vector3.zero;
        levelPlatforms.Clear();
        levelPlatforms = new List<LevelPlatformData>();
        for (int i = 0; i < levelPlatformsObject.Count; i++)
        {
            DestroyImmediate(levelPlatformsObject[i].gameObject);
        }
        DestroyImmediate(parentAllPlatform);
        levelPlatformsObject = new List<Transform>();
        parentAllPlatform = null;
    }
    public void CreateOnePlatform(bool isUp, float heightUp, float widthPit, int width)
    {
        var platformData = new LevelPlatformData("Platform", indexPlatform, width, isUp, heightUp, widthPit, isFirstPlatform, positionNextPlatform);
        CreatePlatformFromData(platformData);
    }
    public void CreatePlatformFromData(LevelPlatformData levelPlatformData)
    {
        levelPlatformData.Index = indexPlatform;
        var platform = new GameObject($"Platform ({indexPlatform})");
        if (levelPlatformData.IsFirstPlatform)
        {
            positionNextPlatform = levelPlatformData.ParentPosition;
            isFirstPlatform = false;
        }
        if (levelPlatformData.IsUpPlatform)
        {
            var posYNextPlatform = positionNextPlatform.y;
            positionNextPlatform.y += levelPlatformData.HeightUp;
            platform.transform.position = positionNextPlatform;
            CreatePlatform(platform, spriteLeftUp, spriteCenterUp, spriteRightUp, levelPlatformData.Width);
            positionNextPlatform.y = posYNextPlatform;
        }
        else
        {
            platform.transform.position = positionNextPlatform;
            CreatePlatform(platform, spriteLeftDown, spriteСenterDown, spriteRightDown, levelPlatformData.Width);
        }

        positionNextPlatform.x += levelPlatformData.WidthPit;
        levelPlatforms.Add(levelPlatformData);
        levelPlatformsObject.Add(platform.transform);
        platform.transform.parent = parentAllPlatform.transform;
        indexPlatform++;
    }
    public void InsertPlatformFromData(LevelPlatformData levelPlatformData, int index)
    {
        positionNextPlatform = levelPlatformData.ParentPosition;
        levelPlatformData.Index = index;
        var platform = new GameObject($"Platform ({index})");
        if (levelPlatformData.IsUpPlatform)
        {
            var posYNextPlatform = positionNextPlatform.y;
            positionNextPlatform.y += levelPlatformData.HeightUp;
            platform.transform.position = positionNextPlatform;
            CreatePlatform(platform, spriteLeftUp, spriteCenterUp, spriteRightUp, levelPlatformData.Width);
            positionNextPlatform.y = posYNextPlatform;
        }
        else
        {
            platform.transform.position = positionNextPlatform;
            CreatePlatform(platform, spriteLeftDown, spriteСenterDown, spriteRightDown, levelPlatformData.Width);
        }
        positionNextPlatform.x += levelPlatformData.WidthPit;
        levelPlatforms.Insert(index, levelPlatformData);
        levelPlatformsObject.Insert(index, platform.transform);
        platform.transform.parent = parentAllPlatform.transform;
    }
    private void CreatePlatform(GameObject parent, Sprite spriteLeft, Sprite spriteСenter, Sprite spriteRight, int width )
    {
        CreatePartPlatform(0, spriteLeft, parent);
        for (int i = 1; i < width-1; i++)
        {
            CreatePartPlatform(i, spriteСenter, parent);
        }
        CreatePartPlatform(width - 1, spriteRight, parent);
    }
    private void CreatePartPlatform(int indexPart,Sprite sprite, GameObject parent)
    {
        var obj = new GameObject($"sprite {indexPart}");
        var spriteRend = obj.AddComponent<SpriteRenderer>();
        spriteRend.sprite = sprite;
        spriteRend.sortingLayerName = "Platforms";
        var BoxCollider2d = obj.AddComponent<BoxCollider2D>();
        float widthColliderX = BoxCollider2d.size.x;
        var position = positionNextPlatform;
        position.x += (widthColliderX * 0.5f);
        obj.transform.position = position;
        positionNextPlatform.x += widthColliderX;
        obj.transform.parent = parent.transform;
    }
    public static void CreateAsset<T>( T obj, int index) where T : ScriptableObject
    {
        var asset = obj;
        string assetPathAndName = "Assets/LevelData/LevelPlatform/Platform" + index.ToString("D2") + ".asset";
        AssetDatabase.CreateAsset(asset, assetPathAndName);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public static T LoadAsset<T>(int index) where T : ScriptableObject
    {
        T asset;
        string assetPathAndName = "Assets/LevelData/LevelPlatform/Platform" + index.ToString("D2") + ".asset";
        asset = AssetDatabase.LoadAssetAtPath<T>(assetPathAndName);
        return asset;
    }

    public static void DeleteAsset<T>(int index) where T : ScriptableObject
    {
        string assetPathAndName = "Assets/LevelData/LevelPlatform/Platform" + index.ToString("D2") + ".asset";
        AssetDatabase.DeleteAsset(assetPathAndName);
        AssetDatabase.Refresh();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private Transform[] m_Gears;
    [SerializeField] private int[] m_RotateDirection;
    [SerializeField] private float[] m_Speed;
    [SerializeField] private Text[] m_TextsBlinking;
    [SerializeField] private Image m_FadeImage;
    [SerializeField] private GameObject[] m_ObjectFalse;
    [SerializeField] private Camera camera;
    private bool fadeIn;
    private bool fadeOut;
    float alphaColor;

    private bool[] flagBlinking;
    private float upperLimit;
    private float speedcolor =0.5f;
    private bool position = false;
    private bool positionBack = false;

    private static string nextLevel;

    public static void LoadLevel(string level)
    {
        nextLevel = level;
        SceneManager.LoadScene("LoadingScene");
        
    }

    private IEnumerator Start()
    {
        GameManager.SetGameState(GameState.Loading);

        flagBlinking = new bool[m_TextsBlinking.Length];
        upperLimit=1f/ (m_TextsBlinking.Length-1f);


        yield return new WaitForSeconds(3f);
        positionBack = true;

        //yield return new WaitForSeconds(3f);
        AsyncOperation loading = null;
        if (string.IsNullOrEmpty(nextLevel))
        {
            loading = SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Additive);
            //SceneManager.LoadScene("MainMenu");
            //yield break;
        }
        else
        {
            loading = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);
        }

        while (!loading.isDone)
        {

            yield return null;
        }
        fadeIn = true;
        while (fadeOut || fadeIn)
        {
            yield return null;
        }
        nextLevel = null;
        SceneManager.UnloadSceneAsync("LoadingScene");
    }

    private void Update()
    {
        TextBlinking();
        RotateGears();
        if (fadeIn)
        {
            if (m_FadeImage.color.a > 0.99f)
            {
                fadeOut = true;
                fadeIn = false;
                return;
            }
            alphaColor = Mathf.Lerp(m_FadeImage.color.a, 1, Time.deltaTime * 2f);
            var color = m_FadeImage.color;
            color.a = alphaColor;
            m_FadeImage.color = color;
            return;
        }
        if (!(!fadeOut || fadeIn))
        {
            ActiveObjectLoadingScene(false);
        }
        if (fadeOut)
        {
            if (m_FadeImage.color.a < 0.01)
            {
                fadeOut = false;
            }

            alphaColor = Mathf.Lerp(m_FadeImage.color.a, 0, Time.deltaTime * 2f);
            var color = m_FadeImage.color;
            color.a = alphaColor;
            m_FadeImage.color = color;
            return;
        }
    }

    private void ActiveObjectLoadingScene(bool active)
    {
        if (m_ObjectFalse[0].activeSelf)
        {
            for (int i = 0; i < m_ObjectFalse.Length; i++)
            {
                m_ObjectFalse[i].SetActive(active);
            }
            var colorBackgroundCamera = camera.backgroundColor;
            colorBackgroundCamera.a = 0f;
        }
    }
    private void RotateGears()
    {
        if (PositionGears(position, 0f, 10))
        {
            for (int i = 0; i < m_Gears.Length; i++)
            {
                m_Gears[i].parent.transform.Rotate(Vector3.back, -60 * Time.deltaTime);
            }
            return;
        }
        if (!position)
        {
            var rotation = m_Gears[0].parent.transform.rotation;
            rotation.eulerAngles = new Vector3(0f, 0f, 0f);
            for (int i = 0; i < m_Gears.Length; i++)
            {
                m_Gears[i].parent.transform.rotation = rotation;
            }
            position = true;
            return;
        }
        if (!positionBack)
        {
            for (int i = 0; i < m_Gears.Length; i++)
            {
                m_Gears[i].Rotate(Vector3.back, m_Speed[i] * m_RotateDirection[i] * Time.deltaTime);
            }
            return;
        }
        if (PositionGears(false, 290, 300))
        {
            for (int i = 0; i < m_Gears.Length; i++)
            {
                m_Gears[i].parent.transform.Rotate(Vector3.back, 60 * Time.deltaTime);
            }
            return;
        }
    }

    private bool PositionGears(bool positionGears, float anglesOne, float anglesTwo)
    {
        if (positionGears)
        {
            return false;
        }
        for (int i = 0; i < m_Gears.Length; i++)
        {
            if (m_Gears[i].parent.transform.rotation.eulerAngles.z> anglesOne && m_Gears[i].parent.transform.rotation.eulerAngles.z < anglesTwo)
            {
                return false;
            }  
        }
        return true;
    }

    private void TextBlinking()
    {
        for (int i = 0; i < m_TextsBlinking.Length; i++)
        {
            if (flagBlinking[i])
            {
                continue;
            }
            if (m_TextsBlinking[i].color.r>0.99f)
            {
                flagBlinking[i] = true;
                continue;
            }
            var color = speedcolor * Time.deltaTime;
            var colorText = m_TextsBlinking[i].color;
            if (i!=0)
            {
                print($"{m_TextsBlinking[i - 1].color.r}");
                if (m_TextsBlinking[i - 1].color.r < upperLimit)
                {
                    continue;
                }
            }
            colorText.r += color;
            colorText.b += color;
            colorText.g += color;
            m_TextsBlinking[i].color = colorText;
        }

        for (int i = 0; i < m_TextsBlinking.Length; i++)
        {
            if (!flagBlinking[i])
            {
                continue;
            }
            if (m_TextsBlinking[i].color.r < 0.01f)
            {
                flagBlinking[i] = false;
                continue;
            }
            var color = speedcolor * Time.deltaTime;
            var colorText = m_TextsBlinking[i].color;
            colorText.r -= color;
            colorText.b -= color;
            colorText.g -= color;
            m_TextsBlinking[i].color = colorText;
        }
    }






}

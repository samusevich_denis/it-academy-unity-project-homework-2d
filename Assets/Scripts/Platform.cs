﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private bool isAnimated;
    [SerializeField] private float animationDistance = 5f;
    private bool isEffector;
    private SurfaceEffector2D surfaceEffector;

    private float startX;
    private void Start()
    {
        startX = transform.position.x;
        if (isAnimated)
        {
            StartCoroutine(AnimationProcess());
        }
        isEffector = GetComponent<SurfaceEffector2D>();
        if (isEffector)
        {
            surfaceEffector= GetComponent<SurfaceEffector2D>();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        bool isMovedObject = other.GetComponent<CharacterMovement>();
        if (isMovedObject)
        {
            other.transform.parent = transform;
        }
         if (isEffector)
        {
            surfaceEffector.speed = Random.Range(-5f, 5f);
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.parent == transform)
        {
            other.transform.parent = null;
        }
    }

    private IEnumerator AnimationProcess()
    {
        var rightMovement = true;
        var delta = 0f;

        while (true)
        {
            delta += Time.deltaTime;

            if (delta > 1f)
            {
                delta = 0;
                rightMovement = !rightMovement;
            }

            Vector3 position = transform.position;
            var from = rightMovement ? startX : startX + animationDistance;
            var to = rightMovement ? startX + animationDistance : startX;
            position.x = Mathf.Lerp(from, to, delta);
            transform.position = position;
            yield return null;
        }
    }
}

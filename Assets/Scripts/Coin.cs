﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private float movementSpeed = 7f;

    private CanvasManager canvasManager;
    private Image canvasImageCoin;
    private Coroutine moveUpCorotine;
    private Vector3 vectorToCoin;
    private void Start()
    {
        canvasManager = FindObjectOfType<CanvasManager>();
        canvasImageCoin = canvasManager.GetImageCoin();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (moveUpCorotine == null && other.GetComponent<Player>())
        {
            moveUpCorotine = StartCoroutine(MoveUp());
        }
    }

    private IEnumerator MoveUp()
    {
        animator.SetTrigger("Rotate");
        Destroy(GetComponent<Rigidbody2D>());
        var colliders2D = GetComponents<CircleCollider2D>();
        for (int i = 0; i < colliders2D.Length; i++)
        {
            colliders2D[i].enabled = false;
        }
        transform.parent = canvasManager.transform;
        while (Vector3.Distance(GetVectorXY(transform.position), GetVectorXY(canvasImageCoin.gameObject.transform.position)) > 0.1)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.Translate(vectorToCoin * movementSpeed * Time.deltaTime);
            yield return null;
        }
        canvasManager.CoinPlus();
        Destroy(gameObject);
    }

    private void Update()
    {
        if (moveUpCorotine==null)
        {
            return;
        }
        vectorToCoin = GetVectorXY(canvasImageCoin.gameObject.transform.position) - GetVectorXY(transform.position);
    }
    private Vector3 GetVectorXY(Vector3 position)
    {
        return new Vector3(position.x, position.y, 0f);
    }
}

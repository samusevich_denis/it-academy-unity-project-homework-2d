﻿using UnityEngine;
public class PlayerWeapon : MonoBehaviour, IDamager
{
    [SerializeField] private WeaponData weaponData;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private string buttonName;
    [SerializeField] private Animator animator;
    private float forceWeapon;
    private static float lastAttackTime;
    private static readonly int MeleeAttack = Animator.StringToHash("MeleeAttack");
    private static readonly int RangeAttack = Animator.StringToHash("RangeAttack");
    private static readonly int IsRangeAttack = Animator.StringToHash("IsRangeAttack");
    public int Damage => weaponData.WeaponDamage;
    public void SetDamage()
    {
        if (Time.time - lastAttackTime < weaponData.FireRate)
        {
            animator.SetBool(IsRangeAttack, false);
            return;
        }

        lastAttackTime = Time.time;
        if (weaponData.IsRangeAttack)
        {
            animator.SetTrigger(RangeAttack);
            StartProjectile(forceWeapon);
            animator.SetBool(IsRangeAttack, false);
        }
        else
        {
            animator.SetTrigger(MeleeAttack);
            var target = GetTarget();
            target?.Hit(Damage);
        }
    }

    public string ButtonName => buttonName;

    private IHitBox GetTarget()
    {
        IHitBox target = null;
        RaycastHit2D hit = Physics2D.Raycast(attackPoint.position, attackPoint.right, weaponData.WeaponRange);
        if (hit.collider != null)
        {
            target = hit.transform.gameObject.GetComponent<IHitBox>();
        }
        return target;
    }
    private void StartProjectile(float force)
    {
        var obj = Instantiate(weaponData.Projectile, attackPoint.position, attackPoint.rotation);
        var projectileMovement = obj.GetComponent<ProjectileMovement>();
        var vectorDirection = attackPoint.InverseTransformDirection(new Vector2(1f, 1f));
        projectileMovement.LaunchBullet(force, Damage, vectorDirection);
    }
    public void SetForce(float force)
    {
        forceWeapon = force;
    }
}
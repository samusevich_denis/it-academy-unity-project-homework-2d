﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] private Text canvasCountCoins;
    [SerializeField] private Image imageCoin;
        
        
    private int countCouns;
    // Start is called before the first frame update
    void Start()
    {
        countCouns = 0;
        canvasCountCoins.text = countCouns.ToString("D3");
    }

    public void CoinPlus()
    {
        countCouns ++;
        canvasCountCoins.text = countCouns.ToString("D3");
    }
    public Image GetImageCoin()
    {
        return imageCoin;
    }
}

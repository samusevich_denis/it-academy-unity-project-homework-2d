﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private bool IsRandom;
    [SerializeField] private float spawnTime = 1f;
    private int current = 0;

    void Start()
    {
        StartCoroutine(ObjectsCreatorProcess());
    }

    private IEnumerator ObjectsCreatorProcess()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);
            if (IsRandom)
            {
                current = Random.Range(0, prefabs.Length);
            }

            GameObject obj = Instantiate(prefabs[current]);
            obj.transform.position = transform.position;

            if (IsRandom)
            {
                continue;
            }

            current++;
            if (current >= prefabs.Length)
            {
                current = 0;
            }
        }
    }
}
